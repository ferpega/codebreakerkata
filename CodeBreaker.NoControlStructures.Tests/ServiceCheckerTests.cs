﻿using CodeBreaker.CommonServices;
using CodeBreaker.NoControlStructures;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace CodeBreaker.Tests
{
    [TestFixture]
    public class ServiceCheckerTests
    {
        private Mock<IGeneratorKeysService> _generatorKeyService;

        [SetUp]
        public void BeforeAllTests()
        {
            _generatorKeyService = new Mock<IGeneratorKeysService>();
            _generatorKeyService.Setup(g => g.Generate()).Returns("RAMV");            
        }

        [TestCase("NNNN", "")]
        [TestCase("RNNN", "*")]
        [TestCase("RANN", "**")]
        [TestCase("RAMN", "***")]
        [TestCase("RAMV", "****")]
        public void Check_ReturnsRightNumberOfAsteriscsFromExactPositions(string attemp, string expectation)
        {
            var key = "RAMV";

            var sut = new ServiceChecker(_generatorKeyService.Object);

            sut.Check(attemp).Should().Be(expectation);
        }

        [TestCase("ANNN", "X")]
        [TestCase("AMNN", "XX")]
        [TestCase("AMVN", "XXX")]
        [TestCase("AMVR", "XXXX")]
        public void Check_ReturnsRighNumberOfXCharsFromDifferentPositions(string attemp, string expectation)
        {
            var sut = new ServiceChecker(_generatorKeyService.Object);

            sut.Check(attemp).Should().Be(expectation);
        }

        [TestCase("RNAN", "X*")]
        [TestCase("RRVV", "XX**")]
        public void Check_ReturnsRightResultComplete(string attemp, string expectation)
        {
            var sut = new ServiceChecker(_generatorKeyService.Object);

            sut.Check(attemp).Should().Be(expectation);
        }

        // Cuidado porque los ejemplos en Soolvet no son correctos según el enunciado.
        // http://www.solveet.com/exercises/Kata-CodeBreaker/14
        //
        [TestCase("RANI", "YNYI", "X*")]
        [TestCase("RANI", "RMVI", "**")]
        [TestCase("NRRI", "RRVN", "XX*")]
        public void Check_ReturnsSamplesFromSolveet(string key, string attemp, string expectation)
        {
            _generatorKeyService.Setup(g => g.Generate()).Returns(key);
            var sut = new ServiceChecker(_generatorKeyService.Object);

            sut.Check(attemp).Should().Be(expectation);
        }
    }
}
