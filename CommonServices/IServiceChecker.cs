﻿namespace CodeBreaker.CommonServices
{
    public interface IServiceChecker
    {
        string Check(string attemp);
    }
}