﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeBreaker.CommonServices
{
    public class GeneratorKeysService: IGeneratorKeysService
    {
        public const string VALIDCOLORS = "RAMVNI";
        private const int VALIDKEYLENGTH = 4;

        public string Generate()
        {
            var rnd = new Random();

            var result = string.Empty;
            for (int i = 0; i < VALIDKEYLENGTH; i++)
            {
                result += VALIDCOLORS[rnd.Next(VALIDCOLORS.Length - 1)];
            }

            return result;
        }
    }
}
