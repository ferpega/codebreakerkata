﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeBreaker.CommonServices
{
    public interface IGeneratorKeysService
    {
        string Generate();
    }
}
