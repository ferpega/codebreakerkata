﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeBreaker.CommonServices;

namespace CodeBreaker.NoControlStructures
{
    public class ServiceChecker: IServiceChecker
    {
        private readonly string _key;
        private readonly string[,] values;
        private const int ASTERISC_INDEX = 0;
        private const int X_INDEX = 1;

        public ServiceChecker(IGeneratorKeysService generatorKeysService)
        {
            _key = generatorKeysService.Generate();
            values = new string[2,2];

            values[0,0] = "";
            values[0, 1] = "*";
            values[1, 0] = "";
            values[1, 1] = "X";
        }

        public string Check(string attemp)
        {
            var attempArray = attemp.ToCharArray();

            return string.Format("{1}{0}", CheckForExactPositions(ref attempArray), CheckForDiffPositions(attempArray));
        }


        private string CheckForExactPositions(ref char[] attempArray)
        {
            var result = string.Empty;

            for (var i = 0; i < attempArray.Length; i++)
            {
                var isCharInExactPosition = IsCharInExactPosition(attempArray[i], i);
                attempArray[i] = GetDotIfCharInExactPosition(isCharInExactPosition, attempArray[i]);
                result += GetAppropiateCharFromTrue(ASTERISC_INDEX, isCharInExactPosition);
            }

            return result;
        }
        private bool IsCharInExactPosition(char c, int i)
        {
            return _key.ToCharArray()[i] == c;
        }
        private char GetDotIfCharInExactPosition(bool isCharInExactPosition, char ch)
        {
            var charInPosition = new char[2];
            charInPosition[0] = ch;
            charInPosition[1] = '.';

            return charInPosition[Convert.ToInt32(isCharInExactPosition)];
        }



        private string CheckForDiffPositions(char[] attempArray)
        {
            var result = string.Empty;

            for (var i = 0; i < attempArray.Length; i++)
            {
                var isCharInDiffPosition = IsCharInDiffPosition(attempArray[i], i);
                result += GetAppropiateCharFromTrue(X_INDEX, isCharInDiffPosition);
            }
            return result;
        }
        private bool IsCharInDiffPosition(char c, int i)
        {
            var result = false;
            for (int j = 0; j < _key.Length; j++)
            {
                result = result || c == _key[j];
            }
            return result;
        }


        private string GetAppropiateCharFromTrue(int index, bool withAsterisc)
        {
            return values[index, Convert.ToInt32(withAsterisc)];
        }
    }
}
