﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeBreaker;
using CodeBreaker.CommonServices;
using FluentAssertions;
using NUnit.Framework;

namespace CodeBreaker.CommonServices.Tests
{
    [TestFixture]
    public class GeneratorKeyTests
    {
        [Test]
        public void Generate_GivesAValidKey()
        {
            var sut = new GeneratorKeysService();

            var key = sut.Generate();

            key.Length.Should().Be(4);
            key.ToCharArray().Should().OnlyContain(k => GeneratorKeysService.VALIDCOLORS.Contains(k));
        }
    }
}
