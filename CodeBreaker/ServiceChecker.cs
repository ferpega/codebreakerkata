﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeBreaker.CommonServices;

namespace CodeBreaker
{
    public class ServiceChecker : IServiceChecker
    {
        private readonly char[] keyArray;

        public ServiceChecker(IGeneratorKeysService generatorKeysService)
        {
            this.keyArray = generatorKeysService.Generate().ToCharArray();
        }

        public string Check(string attemp)
        {
            var attempArray = attemp.ToCharArray();

            var result = string.Format("{1}{0}",
                                       GetCharsInPositionResult(ref attempArray),
                                       GetCharsInDifPositionResult(attempArray));
            return result;
        }


        private string GetCharsInPositionResult(ref char[] attempArray)
        {
            attempArray = attempArray
                            .Select((attempChar, index) => keyArray[index] == attempChar ? '.' : attempChar)
                            .ToArray();

            return "".PadLeft(attempArray.Count(a => a == '.'), '*');
        }

        private string GetCharsInDifPositionResult(char[] attempArray)
        {
            var result = attempArray
                            .Count(attempChar => keyArray.Contains(attempChar));

            return "".PadLeft(result, 'X');
        }
    }
}
