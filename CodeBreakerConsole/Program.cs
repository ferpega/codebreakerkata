﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodeBreaker;
using CodeBreaker.CommonServices;

namespace CodeBreaker.ClientConsole
{
    class Program
    {
        private static ServiceChecker _serviceChecker;

        static void Main(string[] args)
        {
            StartServiceChecker();

            Console.WriteLine("***********************************");
            Console.WriteLine("****        CODE BREAKER       ****");
            Console.WriteLine("***********************************");
            Console.WriteLine("*                                 *");
            Console.WriteLine("* I already have choosen a key,   *");
            Console.WriteLine("* please guest it.                *");
            Console.WriteLine("*                                 *");
            Console.WriteLine("***********************************");
            Console.WriteLine("Valid colors: {0}", GeneratorKeysService.VALIDCOLORS);
            Console.WriteLine();
            var match = false;
            while (!match)
            {
                var attemp = Console.ReadLine();
                var result = _serviceChecker.Check(attemp);
                match = result == "****";
                if (!match)
                    Console.WriteLine("{0}   Try again....", result);
            }
            Console.WriteLine(" Congratulations... You win.");
        }

        private static void StartServiceChecker()
        {
            _serviceChecker = new ServiceChecker(new GeneratorKeysService());
        }
    }
}
